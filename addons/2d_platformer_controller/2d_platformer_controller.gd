extends "2d_platformer_kinematicbody.gd"

## actions
export(String) var action_left="ui_left"
export(String) var action_right="ui_right"
export(String) var action_jump="ui_jump"

func _get_inputs():
	return {
		'walk_left' : Input.is_action_pressed(action_left),
		'walk_right' : Input.is_action_pressed(action_right),
		'jump' : Input.is_action_pressed(action_jump),
	}
